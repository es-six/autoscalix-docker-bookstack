# ![Autoscalix logo](https://gitlab.com/es-six/autoscalix-docker-bookstack/-/raw/master/assets/autoscalix.png) Docker BookStack by Autoscalix

**WARNING** : this projects will end shortly :
- Docker images will stay hosted on docker hub
- Repository will be switched to readonly

General tags :
[
![Docker Pulls](https://img.shields.io/docker/pulls/autoscalix/bookstack)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/autoscalix/bookstack/latest)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/autoscalix/bookstack)
![Unmaintained](https://img.shields.io/badge/maintained-no-red)
![Automated builds status](https://img.shields.io/badge/automated%20builds-disabled-red)
](https://gitlab.com/es-six/autoscalix-docker-bookstack)

Dependencies related tags :
[
![Is debian based](https://img.shields.io/badge/image%20based%20on-debian%20bullseye%20(v11)-brightgreen)
![wkhtmltopdf version](https://img.shields.io/badge/wkhtmltopdf-v0.12.6-brightgreen)
![PHP version](https://img.shields.io/badge/PHP-v8.0-brightgreen)
](https://gitlab.com/es-six/autoscalix-docker-bookstack)

License : [
![License type](https://img.shields.io/badge/license-MIT-black)
](https://gitlab.com/es-six/autoscalix-docker-bookstack/-/blob/master/LICENSE)

## 🚀 Description

A stable and reliable Docker image for the BookStack documentation platform, docker image provided for free by Autoscalix.

![bookstack logo](https://gitlab.com/es-six/autoscalix-docker-bookstack/-/raw/master/assets/bookstack.png)

## ⚙️ Supported tags
- `latest`, `amd64-latest`, `arm64-latest`, `armhf-latest`
- `[bootstack released version]`, `amd64-[bootstack released version]`, `arm64-[bootstack released version]`, `armhf-[bootstack released version]` (eg : `v21.05.2`, `amd64-v21.05.2`, `arm64-v21.05.2`, `armhf-v21.05.2`)

More information about BookStack releases here : [https://github.com/BookStackApp/BookStack/releases](https://github.com/BookStackApp/BookStack/releases)

## ⚙️ Supported Architectures

Our images support most common architectures such as `x86-64`, `arm64` and `armhf`.
We use docker manifest for multi-platform awareness.
More information about this feature is available in the docker documentation [here](https://github.com/docker/distribution/blob/master/docs/spec/manifest-v2-2.md#manifest-list).

Simply pulling `autoscalix/bookstack` should retrieve the correct image for your arch, but you can also pull specific arch images via tags.

The architectures supported by this image are:

| Architecture | Tag |
| :----: | --- |
| x86-64 (INTEL/AMD 64 bit) | amd64-latest |
| arm64 (arm64v8) | arm64-latest |
| armhf (arm32v7) | armhf-latest |

If needed, you can pull image for specific architecture using these tags.

Eg : `autoscalix/bookstack:amd64-latest`, `autoscalix/bookstack:arm64-latest` or `autoscalix/bookstack:armhf-latest`

## 🚀 Exemple usage

#### docker-compose (recommended)

```yaml
version: "3.2"
services:
  bookstack:
    image: autoscalix/bookstack
    container_name: bookstack
    environment:
      - APP_URL=https://your-sub.domain.tld
      - DB_HOST=<yourdbhost>
      - DB_USERNAME=<yourdbusername>
      - DB_PASSWORD=<yourdbpass>
      - DB_DATABASE=<yourdbname>
      - DB_PORT=<yourdbport>
      - WAIT_DB_INIT=1
    volumes:
      - /host/data/path/bookstack:/config
    ports:
      - 8000:80
    restart: unless-stopped
    depends_on:
      - bookstack_db
  bookstack_db:
    image: mysql
    container_name: bookstack_db
    ports:
      - <yourdbport>:3306
    environment:
      - TZ=Europe/Paris
      - MYSQL_ROOT_PASSWORD=<yourdbpass>
      - MYSQL_DATABASE=<yourdbname>
      - MYSQL_USER=<yourdbusername>
      - MYSQL_PASSWORD=<yourdbpass>
    command: mysqld --default-authentication-plugin=mysql_native_password
    volumes:
      - /host/data/path/mysql:/var/lib/mysql
    restart: unless-stopped
```

#### docker cli

```bash
docker run -d \
  --name=bookstack \
  -e APP_URL=https://your-sub.domain.tld \
  -e DB_HOST=<yourdbhost> \
  -e DB_USERNAME=<yourdbuser> \
  -e DB_PASSWORD=<yourdbpass> \
  -e DB_DATABASE=<yourdbname> \
  -p 8000:80 \
  -v /path/to/data/on/host:/config \
  --restart unless-stopped \
  autoscalix/bookstack
```

## 📖 Base parameters

Autoscalix docker images are configured so the BookStack .env parameters can be passed at runtime (such as those above).

| Parameter | Function |
| :----: | --- |
| `-p 8000:80` | will map the container's port 80 to port 8000 on the host |
| `-e APP_URL=` | for specifying the IP:port or URL your BookStack instance will be accessed on (ie. `http://192.168.1.123:8000` or `https://bookstack.mydomain.tld` |
| `-e DB_HOST=<yourdbhost>` | for specifying the database host |
| `-e DB_USERNAME=<yourdbuser>` | for specifying the database user |
| `-e DB_PASSWORD=<yourdbpass>` | for specifying the database password |
| `-e DB_DATABASE=<yourdbname>` | for specifying the database to be used |
| `-e APP_KEY=<yourSecretAppKeyHere>` | (optional) this key is used by BookStack for ciphering purposes (CSRF and session cookies), if no APP_KEY specified, it will be generated automatically. The APP_KEY needs to be 16 or 32 characters long |
| `-e WAIT_DB_INIT=1` | (optional) for making container to wait the database to be available, this will wait database to be up and running for host and port specified in environment variables DB_HOST AND DB_PORT. (usefull if you use mysql database and BookStack in a docker-compose file). If you use an external database (already started when the BookStack container start), don't specify this variable to make container start faster. |
| `-v </path/to/data/on/host>:/config` | (optional if you use an object storage with BookStack, eg : S3) this will store any uploaded data on the docker host (for production, I recommend using an S3 object storage, more information about storing BookStack uploaded files into an object storage in the BookStack documentation [here](https://www.bookstackapp.com/docs/admin/upload-config/#s3)) |


## 🛠️ Platform Setup

BookStack has the following default credentials :
- username : admin@admin.com
- password : password

#### Advanced usage

If you need to use the extra functionalities of BookStack (email, Memcache, LDAP, etc.) you may need to pass additional variables in the docker container environment (or make your own .env file).

You can pass any of the BookStack .env.example.complete variables as additional container environment variables with docker run (eg : `-e VARIABLE=value`).

Check available .env variables for BookStack [here](https://github.com/BookStackApp/BookStack/blob/master/.env.example.complete).

You can check the BookStack documentation for additional information about the .env file.

When you create the container, the image will use the .env file in the path `/config/www/.env`.

#### PDF Rendering

A stable version of [wkhtmltopdf](https://wkhtmltopdf.org/) (with patched QT) is provided in this image, you can use it as an alternative PDF rendering generator with BookStack as described in the BookStack documentation [here](https://www.bookstackapp.com/docs/admin/pdf-rendering/).

This wkhtmltopdf binary has been tested to render successfully a large amount of bookstack pages (more than 100) with various content without crashing (if you have enought free RAM of course).

The path to the wkhtmltopdf binary in this image to include in your .env file is `/usr/local/bin/wkhtmltopdf`.

Eg : to enable wkhtmltopdf rendering, use `WKHTMLTOPDF=/usr/local/bin/wkhtmltopdf` as environment variable.

#### Tips for hosting BookStack in a cluster

This image is also compatible with common docker cloud hosting technologies (eg : kubernetes, ECS, ...) if you need to host BookStack on a fault tolerant and scalable infrastructure.

If you want to host BookStack behind a load balancer, you will need to use cookie based sticky sessions for CSRF and login to work properly.

I recommend to base sticky sessions on the XSRF-TOKEN cookie as it allow each requests (with a unique CSRF token) to be runned on different server if needed instead of stick to one server for the whole session.

## 🌐 About updates

New version of Autoscalix docker images are tested automaticaly then pushed to Docker Hub by a scheduled gitlab pipeline for each new BookStack release since v21.04

BookStack updates are checked every day.

Here is the list of dependencies and their current version embeded in this docker image :
- wkhtmltopdf : version 0.12.6-1
- debian base image : bullseye-slim:latest (debian v11)
- PHP : verison 8.0.x

You can find bellow an exemple of how to update your docker-bookstack installation depending of how you run BookStack in docker (docker-compose or docker run).

Database migrations are done during the container startup.

#### docker-compose

```bash
# Update images, then update containers
docker-compose pull
docker-compose up -d

# Optionally, remove old dandling images
docker image prune
```

#### docker cli

```bash
# Update the autoscalix/bookstack image
docker pull autoscalix/bookstack

# Then stop the container
docker stop bookstack

# Remove old dandling images
docker rm bookstack

# Then recreate a container from the docker images using the docker run exemple above (see Exemple usage with docker cli)
...

# Finally remove old dandling images
docker image prune
```

## 🧰 Building locally

If you need to make advanced modifications to these images for development or other purposes, use the following commands to build this image locally :

```bash
git clone https://gitlab.com/es-six/autoscalix-docker-bookstack.git autoscalix-docker-bookstack
cd autoscalix-docker-bookstack
git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch bookstack
docker build --no-cache -t autoscalix/bookstack .
```

The `arm64` image can be built on x86_64 hardware using [docker buildx](https://docs.docker.com/buildx/working-with-buildx/)

```bash
# Get qemu for Docker
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
# Create a docker buildx builder
docker buildx create --name multiarch-builder
docker buildx use multiarch-builder
docker buildx inspect --bootstrap multiarch-builder
# Run build
docker buildx build --load --no-cache --platform linux/arm64 -t bookstack-arm64 .
```

The `armhf` image can be built on x86_64 hardware using [docker buildx](https://docs.docker.com/buildx/working-with-buildx/)

```bash
# Get qemu for Docker
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
# Create a docker buildx builder
docker buildx create --name multiarch-builder
docker buildx use multiarch-builder
docker buildx inspect --bootstrap multiarch-builder
# Run build
docker buildx build --load --no-cache --platform linux/arm/v7 -t bookstack-armhf .
```

Use the default Dockerfile, it can be use to build x86_64, arm64 or armhf build.

## 🤖 Usage on Windows with WSL

If you want to use this image on windows WSL, it's possible if you do two extra things :
- Use host.docker.internal as host for your database.
- Store your docker volumes inside the WSL and not on the Windows NTFS filesystem, because it doesn't have the same permission managment system as EXT4 on Linux, else you will get errors while accessing files in your volume.

Recommandation : use the docker-compose example above and specify DB_HOST=host.docker.internal

## 🔨 Troubleshooting

If you need to display errors occurring in BookStack to troubleshoot your installation, pass the variable APP_DEBUG=1 as environment variable so error details will be displayed when error occur while loading a page.

## 📚 Source code

You can find the Gitlab repository here : [https://gitlab.com/es-six/autoscalix-docker-bookstack](https://gitlab.com/es-six/autoscalix-docker-bookstack)

## News

- 07/09/2021 : upgrade to debian 11 (bullseye)

## 📚 Licence

[MIT](https://gitlab.com/es-six/autoscalix-docker-bookstack/-/blob/master/LICENSE), checkout the Gitlab repository : [here](https://gitlab.com/es-six/autoscalix-docker-bookstack/-/blob/master/LICENSE)
