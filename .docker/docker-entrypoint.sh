#!/usr/bin/env bash

/etc/init.d/php8.0-fpm start

# Initialise upload folders
mkdir -p /config/www/{uploads,files,images}

SYMLINKS_TO_CREATE=( \
/var/www/storage/uploads/files \
/var/www/storage/uploads/images \
/var/www/public/uploads \
/var/www/.env \
/var/www/storage/logs/laravel.log
)

for i in "${SYMLINKS_TO_CREATE[@]}"
do
[[ -e "$i" && ! -L "$i" ]] && rm -rf "$i"
[[ ! -L "$i" ]] && ln -s /config/www/"$(basename "$i")" "$i"
done

if [[ -z $APP_KEY ]]; then
    export APP_KEY=$(php8.0 /var/www/artisan key:generate --show)
fi

chown -R www-data:www-data /config

if [ -z "$WAIT_DB_INIT" ]
then
	echo "Container FAST START mode ENABLED : container will consider database is up and running now. Add environment variable WAIT_DB_INIT=1 if you need container to wait for database to start."
else
    echo "Container FAST START mode DISABLED : container will wait for database to be up and running. Remove environment variable WAIT_DB_INIT=1 if you need container to start faster without checking database availability."
	echo "Waiting for database to be up..."
	export NB_SUCCESS=0
	export NB_ERRORS=0
	until [ "$NB_SUCCESS" -ge 5 ]
	do
		echo "Trying to check if database is up and running..."
		# This mode is usefull when using database and this container in a docker compose file.
		# Because the database isn't available immediately but docker consider the database container is healthy si we need to wait database is fully up and running before migrating database
		# The trick here is to run artisan migrate:install to try to create the migration table if it doesn't exist already
		# Then we try to get the status of existing migrations
		# If it succeed, the test is counted as a success and wait 3 second before running next test.
		# Database is considered up and running after 5 consecutive successes
		su -pc www-data -c "php8.0 /var/www/artisan migrate:install" > /dev/null 2>&1
		su -pc www-data -c "php8.0 /var/www/artisan migrate:status" > /dev/null 2>&1
		export RETURN_CODE=$?

		if [ "$RETURN_CODE" -ge 1 ]
		then
			echo "Failed to connect, retrying in 3 seconds..."
			export NB_SUCCESS=0
			export NB_ERRORS=$((NB_ERRORS+=1))
			sleep 3
		else
			export NB_SUCCESS=$((NB_SUCCESS+=1))
			export NB_ERRORS=0
			echo "Connection to database succeed (test $NB_SUCCESS / 5)"
			sleep 3
		fi
		
		# We allow maximum 120 seconds for database to be up and running
		# 40 fails and 3 seconds wait between each take ~120 seconds
		# if 40 consecutive fails occured then stop container
		if [ "$NB_ERRORS" -ge 40 ]
		then
    		su -pc www-data -c "php8.0 /var/www/artisan migrate:status"
			echo "Error : database seem's to be unavailable. Please check database availability and database configuration then try again"
			exit 1
		fi
	done
fi

su -pc www-data -c "php8.0 /var/www/artisan migrate --force"

apache2ctl -DFOREGROUND
