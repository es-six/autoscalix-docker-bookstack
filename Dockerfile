FROM composer:latest
FROM debian:bullseye-slim

WORKDIR /var/www

ADD .docker/docker-entrypoint.sh /docker-entrypoint.sh

COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY bookstack ./bookstack

RUN apt-get update \
	&& apt-get install --no-install-recommends -qq -y curl wget gnupg2 openssl lsb-release apt-transport-https ca-certificates \
	&& wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - \
	&& echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list \
	&& apt-get update \
	&& apt-get install --no-install-recommends -qq -y apache2 php8.0 php8.0-mysql php8.0-curl php8.0-fpm php8.0-gd php8.0-mbstring php8.0-zip php8.0-common php8.0-xml \
	&& cp -r bookstack/* /var/www \
	&& rm -rf bookstack \
	&& composer install --ignore-platform-reqs --no-dev --no-interaction --prefer-dist --optimize-autoloader \
	&& a2enmod headers deflate rewrite proxy proxy_fcgi setenvif \
	&& a2ensite 000-default.conf \
	&& chmod +x /docker-entrypoint.sh \
	&& chown -R www-data:www-data /var/www/* \
	&& wget -O wkhtmltox.deb https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.$(uname -m | sed -e 's/aarch64/buster_arm64/g' | sed -e 's/armv7l/raspberrypi.buster_armhf/g' | sed -e 's/x86_64/buster_amd64/g').deb \
	&& chmod 777 ./wkhtmltox.deb \
	&& apt install --no-install-recommends ./wkhtmltox.deb -y \
	&& rm ./wkhtmltox.deb

COPY .docker/site.conf /etc/apache2/sites-available/000-default.conf
COPY .docker/www.conf /etc/php/8.0/fpm/pool.d
COPY .docker/php.ini /etc/php/8.0/fpm
COPY .docker/php.ini /etc/php/8.0/cli

EXPOSE 80
ENTRYPOINT ["/docker-entrypoint.sh"]
